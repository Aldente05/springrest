package com.spring.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * Created by krissadewo on 4/18/14.
 */
@Service
public class BaseService {

    @Autowired
    protected TransactionTemplate transactionTemplate;

}