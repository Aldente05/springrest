package com.spring.core.service;

import com.spring.core.dao.InfoJawabanDAO;
import com.spring.core.entity.InfoJawaban;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by f.putra on 4/19/16.
 */
@Service
public class InfoJawabanService extends BaseService{

    @Autowired
    public InfoJawabanDAO infoJawabanDAO;

    public InfoJawaban findByParam(int user_id, int tryout_id) {
        return infoJawabanDAO.findByParam(user_id, tryout_id);
    }
}