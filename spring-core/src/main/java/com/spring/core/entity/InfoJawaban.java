package com.spring.core.entity;

/**
 * Created by f.putra on 4/19/16.
 */
public class InfoJawaban {

    private int user_id;
    private int tryout_id;
    private int date_time_jawab;
    private String status;
    private String jawaban;
    private String kunci;
    private int no;
    private String kelas;
    private int mp;
    private int level;
    private String pelajaran;
    private String level_sekolah;
    private String username;
    private String email;

    public InfoJawaban(int user_id, int tryout_id, int date_time_jawab, String status, String jawaban, String kunci, int no, String kelas, int mp, int level, String pelajaran, String level_sekolah, String username, String email) {
        this.user_id = user_id;
        this.tryout_id = tryout_id;
        this.date_time_jawab = date_time_jawab;
        this.status = status;
        this.jawaban = jawaban;
        this.kunci = kunci;
        this.no = no;
        this.kelas = kelas;
        this.mp = mp;
        this.level = level;
        this.pelajaran = pelajaran;
        this.level_sekolah = level_sekolah;
        this.username = username;
        this.email = email;
    }

    public InfoJawaban(){

    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getTryout_id() {
        return tryout_id;
    }

    public void setTryout_id(int tryout_id) {
        this.tryout_id = tryout_id;
    }

    public int getDate_time_jawab() {
        return date_time_jawab;
    }

    public void setDate_time_jawab(int date_time_jawab) {
        this.date_time_jawab = date_time_jawab;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getJawaban() {
        return jawaban;
    }

    public void setJawaban(String jawaban) {
        this.jawaban = jawaban;
    }

    public String getKunci() {
        return kunci;
    }

    public void setKunci(String kunci) {
        this.kunci = kunci;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public int getMp() {
        return mp;
    }

    public void setMp(int mp) {
        this.mp = mp;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getPelajaran() {
        return pelajaran;
    }

    public void setPelajaran(String pelajaran) {
        this.pelajaran = pelajaran;
    }

    public String getLevel_sekolah() {
        return level_sekolah;
    }

    public void setLevel_sekolah(String level_sekolah) {
        this.level_sekolah = level_sekolah;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "InfoJawaban{" +
                "user_id=" + user_id +
                ", tryout_id=" + tryout_id +
                ", date_time_jawab=" + date_time_jawab +
                ", status='" + status + '\'' +
                ", jawaban='" + jawaban + '\'' +
                ", kunci='" + kunci + '\'' +
                ", no=" + no +
                ", kelas='" + kelas + '\'' +
                ", mp=" + mp +
                ", level=" + level +
                ", pelajaran='" + pelajaran + '\'' +
                ", level_sekolah='" + level_sekolah + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
