package com.spring.core.dao;

import java.util.List;

/**
 * Created by f.putra on 4/19/16.
 */
public interface BaseDAO<T> {

    T save(final T entity);

    T update(final T entity);

    T delete(final T entity);

    T findById(final Long id);

    T findByParam(final int user_id, int tryout_id);

    /**
     * Get all data with and without any parameter
     */
    List<T> find(Integer offset, Integer limit);


    /**
     * Count all data with and without any parameter
     * <p>You can include the parameter using ${@link T} class
     *
     * @return
     */
    int count(T param);
}
