package com.spring.core.dao;

import com.spring.core.entity.InfoJawaban;

/**
 * Created by f.putra on 4/19/16.
 */
public interface InfoJawabanDAO extends BaseDAO<InfoJawaban> {
}
