package com.spring.core.dao.impl;

import com.spring.core.dao.InfoJawabanDAO;
import com.spring.core.entity.InfoJawaban;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by f.putra on 4/19/16.
 */

@Repository
public class InfoJawabanImpl implements InfoJawabanDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public InfoJawaban save(InfoJawaban entity) {
        String sql = "INSERT INTO " + "Table database" +
                    "(soal,jawaban) " +
                    "VALUES (?,?)";
                    jdbcTemplate.update(sql,
                    entity.getSoal,
                    entity.getJawaban);
        return null;
    }

    @Override
    public InfoJawaban update(InfoJawaban entity) {
        return null;
    }

    @Override
    public InfoJawaban delete(InfoJawaban entity) {
        return null;
    }

    @Override
    public InfoJawaban findById(Long id) {
        return null;
    }

    @Override
    public InfoJawaban findByParam(int user_id, int tryout_id) {
            String sql = "select t1.id, t1.user_id, t1.tryout_id, t1.date_time_jawab, t1.status,t1.jawaban,t1.kunci,t1.no,t1.kelas, t2.mp,t2.level, t3.pelajaran, t4.level as level_sekolah, t5.username, t5.email from tryout_jawaban as t1 join un_tryout as t2 on t1.tryout_id = t2.id join un_mp as t3 on t2.level = t3.id join level as t4 on t2.level = t4.id join users as t5 on t1.user_id = t5.id " +
                    "WHERE t1.user_id= ? " +
                    "AND t1.tryout_id = ?";
            InfoJawaban infoJawaban = jdbcTemplate.queryForObject(sql,
                    new Object[]{user_id, tryout_id}, new InfoRowMapper());

            return infoJawaban;
    }

    @Override
    public List<InfoJawaban> find(Integer offset, Integer limit) {
        return null;
    }


    @Override
    public int count(InfoJawaban param) {
        return 0;
    }

    class InfoRowMapper implements RowMapper<InfoJawaban> {
        @Override
        public InfoJawaban mapRow(ResultSet rs, int i) throws SQLException {
            InfoJawaban info = new InfoJawaban();
            info.setUser_id(rs.getInt("user_id"));
            info.setTryout_id(rs.getInt("tryout_id"));
            info.setDate_time_jawab(rs.getInt("date_time_jawab"));
            info.setStatus(rs.getString("status"));
            info.setJawaban(rs.getString("jawaban"));
            info.setKunci(rs.getString("kunci"));
            info.setNo(rs.getInt("no"));
            info.setKelas(rs.getString("kelas"));
            info.setMp(rs.getInt("mp"));
            info.setLevel(rs.getInt("level"));
            info.setPelajaran(rs.getString("pelajaran"));
            info.setLevel_sekolah(rs.getString("level_sekolah"));
            info.setUsername(rs.getString("username"));
            info.setEmail(rs.getString("email"));
            return info;
        }
    }
}
