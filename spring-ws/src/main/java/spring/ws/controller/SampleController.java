package spring.ws.controller;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by kris on 9/1/2014.
 */
@Controller
public class SampleController extends BaseController {

    @RequestMapping(value = "/hello", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public String printHello(ModelMap model) {
        model.addAttribute("message", "Hello Spring MVC Framework!");

        return "hello";
    }

//    @Autowired
//    private InfoJawabanService infoJawabanService;
//
//    @RequestMapping(value = "/info", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
//    public InfoJawaban getByInfo(@RequestParam("user_id") int user_id, @RequestParam("tryout_id") int tryout_id) {
//        InfoJawaban infos = infoJawabanService.findByParam(user_id, tryout_id);
//
//        return infos;
//    }

//    @RequestMapping(value = "/info/{user_id}", method = RequestMethod.GET)
//    public ResponseEntity<InfoJawaban> find(@PathVariable("user_id") int User_id) {
//        List<InfoJawaban> info = infoJawabanService.find(User_id);
//        if (info == null) {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }
//        return new ResponseEntity<>(HttpStatus.OK);
//    }

//    @RequestMapping(value = "/agamas/", method = RequestMethod.POST)
//    public ResponseEntity<Result> save(@RequestBody Agama agama, HttpServletRequest request, HttpServletResponse response) {
//        Result result = agamaService.save(agama);
//        if (result.getMessage().equals(Result.SAVE_SUCCESS)) {
//            return new ResponseEntity<>(HttpStatus.OK);
//        }
//        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//    }
//
//    @RequestMapping(value = "/agamas/", method = RequestMethod.PUT)
//    public ResponseEntity<Result> update(@RequestBody Agama agama, HttpServletRequest request, HttpServletResponse response) {
//        Result result = agamaService.save(agama);
//        if (result.getMessage().equals(Result.SAVE_SUCCESS)) {
//            return new ResponseEntity<>(HttpStatus.OK);
//        }
//        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//    }
//
//    @RequestMapping(value = "/agamas/", method = RequestMethod.DELETE)
//    public ResponseEntity<Result> delete(@RequestBody Agama agama, HttpServletRequest request, HttpServletResponse response) {
//        Result result = agamaService.save(agama);
//        if (result.getMessage().equals(Result.SAVE_SUCCESS)) {
//            return new ResponseEntity<>(HttpStatus.OK);
//        }
//        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//    }
}
